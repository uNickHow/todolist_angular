angular.module('todoApp', [])
  .controller('TodoListController', function () {
    var todoList = this;
    //seed data
    todoList.todos = [
      {
        text: 'Learn angular.js', 
        done: false
      },
      {
        text: 'Interview',
        done: false
      },
      {
        text: 'Practice Vue.js',
        done: true
      },
      {
        text: 'Read Node.js doc',
        done: false
      }];
    //add item function
    todoList.addTodo = function () {
      if (todoList.todoText) {
        todoList.todos.push({
          text: todoList.todoText,
          done: false
        });
        todoList.todoText = '';
        todoList.blank = '';
      }
      //alert text show up if there's nothing input
      else todoList.blank = 'You need to input something ...'; 
    };
    
    //delete item function
    todoList.delete = function (index) {
      todoList.todos.splice(index, 1)
    };
  });
